import re
import os
import json
import html2text
import unicodedata

def change_val(payload, field_name, new_value):
    #help rof regexp here 
    #https://regex101.com/
    #https://docs.python.org/3/howto/regex.html

    regexp=r'(?P<PRE>.?FIELD_NAME\s*[:=].*?)\w.*$'

    regexp=regexp.replace("FIELD_NAME",field_name)

    matches = re.finditer(regexp, payload, re.MULTILINE)
    for matchNum, match in enumerate(matches, start=1):
        matchline=match.group()
        prestring = match.groups()[0]
        prestring_first_char=prestring[0]
        prestring_last_char=prestring[-1]

        end_char=r"\s"
        #  'Authorization: "Bearer <access_token>"' 
        #  Authorization: 'Bearer <access_token>' 
        if re.match(r'\W',prestring_first_char) and re.match(r'\W',prestring_last_char) and re.match(r'[^=:\s]',prestring_last_char):
            end_char=prestring_last_char

        #  Authorization: Bearer <access_token>
        elif re.match(r'\W',prestring_first_char) and re.match(r'[^\s]',prestring_first_char):
            end_char=prestring_first_char
        
        #Authorization:'Replace ME'
        elif re.match(r'\W',prestring_last_char) and re.match(r'[^=:\s]',prestring_last_char):
            end_char=prestring_last_char

        extractregexp=r"(?P<PRE>.?FIELD_NAME\s*[:=].*?)(?P<VALUE>\w.*?)(?P<AFTER>" + end_char + r".*$)"
        extractregexp=extractregexp.replace("FIELD_NAME",field_name)


        match = re.compile(extractregexp)
        #added spaces at the beginig and end of the payload otherwise it don't match values at beggining or end of the payload
        replaced = match.sub(r'\g<PRE>' + new_value + r'\g<AFTER>', matchline )
        payload = payload.replace(matchline, replaced)
    return payload

def append_after_string(whole_text, string, text_to_add):
    position = whole_text.find(string) + len(string)
    new_text = whole_text[0:position] +  text_to_add + whole_text[position:]
    return new_text


def logf(val, file="log.txt"):
    path=os.path.abspath(file).replace(os.path.basename(file),"")
    if not os.path.isdir(path):
        os.makedirs(path)

    if val ==None and os.path.exists(file):
        os.remove(file)
        with open(file, 'w') as f:
            f.write("")
    else:
        with open(file, 'a+') as f:
            f.write(str(val) + "\n")

def log_attached(val):
    logf(val, "archives/log_attached.txt")

def logj(jsonval, file="log.json"):
    path=os.path.abspath(file).replace(os.path.basename(file),"")
    if not os.path.isdir(path):
        os.makedirs(path)
        
    if jsonval ==None and os.path.exists(file):
        with open(file, 'w') as f:
            f.write("")
    else:
        if os.path.exists(file):
            with open(file, 'r') as f:
                length = len(f.read())
            if length>0:
                with open(file, 'r') as f:
                    jsonvalfile =json.load(f)
            else:
                jsonvalfile=[]
            jsonvalfile.append(jsonval)
            with open(file, 'w') as f:
                json.dump(jsonvalfile, f, indent=2)

        else:
            jsonvallist=[]
            if jsonval!=None:
                jsonvallist.append(jsonval)
                with open(file, 'w') as f:
                    json.dump(jsonvallist, f, indent=2)

def log_attachedj(val):
    logj(val, "log_attached.json")

def log_request_error(output, log, response, action):
    output.printv(response.url)
    output.printv(response.reason)
    output.printv(response.content.decode())
    log["error.action"]=action
    log["error.url"]= response.url
    log["error.reason"]= response.reason
    log["error.content"]= response.content.decode()
    logj(log, "archives/log_failed.json")


class verbose:
    verbose= False
    def __init__(self, verbose=False):
        self.verbose=verbose

    def on(self):
        self.verbose=True

    def off(self):
        self.verbose=False

    def printv(self,val):
        if self.verbose==True:
            print(val)
        logf(val, "log_execution.log")





def parseHTML(data):
    if data!=None and data!="":
        h =html2text.HTML2Text()
        unicode_normalized = unicodedata.normalize("NFKD",h.handle(data))
        filtered_string = unicode_normalized.replace('\u200b', '').replace('\u200d', '').replace('\ufeff', '')
        return filtered_string
    else:
        return ""
