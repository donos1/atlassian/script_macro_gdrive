import urllib.parse

def url_encode(url):
    return urllib.parse.quote(url, safe='=&/:=?_()\'')