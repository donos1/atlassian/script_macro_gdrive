
import requests
import curlparser

class UrlParts():
    __parts=[]
    __url_no_protocol=""
    __domain_index=1
    __url_no_protocol_parts=[]
    url=""
    domain=""
    query={}
    path=""
    def __init__(self,url):
        self.__domain_index=1
        self.url=url
        if url=="":
            return
        self.__parts= list(filter(None,url.split("/")))
        if ":" in self.__parts[0]:
            self.protocol=self.__parts[0].replace(":", "")
        else:
            self.__domain_index=0
            self.protocol=""

        self.__url_no_protocol =   "/".join(self.__parts[self.__domain_index:])

        query_str=""

        self.__url_no_protocol_parts=self.__url_no_protocol.split("?")

        self.domain=""
        if self.url[0]!="/":
            self.domain=self.__parts[self.__domain_index]
        else:
            self.__domain_index=None

        self.path=self.__url_no_protocol_parts[0].replace(self.domain,"")
        if len(self.__url_no_protocol_parts)>1:
            query_str ="?".join(self.__url_no_protocol_parts[1:])
        self.query={}

        if query_str!="":
            setpairlist=query_str.split("&")

            if len(setpairlist)>=1:
                for setpair in setpairlist:
                    if setpair!="":
                        if "=" in setpair:
                            keyvalue=setpair.split("=")
                            self.query[keyvalue[0]]="=".join(keyvalue[1:])
                        else:
                            self.query[setpair]=setpair

    def base_url(self):
        protocol=""
        if self.protocol!="":
            protocol= self.protocol +"://"
            
        domain="/"
        if self.domain!="":
            domain= self.domain

        return protocol + domain

    def to_string(self):
        return self.__call__()

    def set_domain(self, newdomain, protocol=""):
        if protocol!="" or protocol!=None:
            self.protocol=protocol
        if newdomain!=None and newdomain!="":
            if self.__domain_index!=None:
                self.__parts[self.__domain_index]=newdomain
            else:
                self.__domain_index=0
                self.__parts=[newdomain] + self.__parts
            self.__url_no_protocol =   "/".join(self.__parts[self.__domain_index:])
            self.__url_no_protocol_parts=self.__url_no_protocol.split("?")
            self.domain=self.__parts[self.__domain_index]
            self.path=self.__url_no_protocol_parts[0].replace(self.domain,"")
            self.url=self.base_url()

    def set_path(self, newpath):
        self.__url_no_protocol_parts[0]=self.domain + newpath
        self.path=newpath
        protocol_part=""
        if self.protocol:
            protocol_part=self.protocol + "://"
        self.url = protocol_part + self.__url_no_protocol_parts[0] + self.querystring()
        self.__parts= list(filter(None,self.url.split("/")))
        self.__url_no_protocol =   "/".join(self.__parts[self.__domain_index:])


    def set_query(self, newquery_dict):
        self.query=newquery_dict
        protocol_part=""
        if self.protocol:
            protocol_part=self.protocol + "://"
        self.url = protocol_part + self.__url_no_protocol_parts[0] + self.querystring()
        self.__parts= list(filter(None,self.url.split("/")))
        self.__url_no_protocol =   "/".join(self.__parts[self.__domain_index:])

 
    def querystring(self):
        qtring=""
        for key, value in self.query.items():
            qtring= qtring + "&" + str(key) + "=" + str(value)

        if qtring!="":
            qtring="?" + qtring[1:]

        return qtring

    def __call__(self):
        return self.base_url() + self.path + self.querystring()

class ApiProps(object):
    defaut_method="GET"
    defaut_headers={}
    defaut_payload=None
    response = None
    url=UrlParts("")

    def __init__(self, method, full_url_default_end_point, headers, payload):
        self.defaut_method=method.upper()
        self.url=UrlParts(full_url_default_end_point)
        self.defaut_headers=headers
        self.defaut_payload=payload

    def request(self):
        try:
            self.response = requests.request(self.defaut_method, self.url(), headers=self.defaut_headers, data=self.defaut_payload)
            return self.response
        except Exception as e:
            class reqq:
                status_code=str(e)
                url=self.url()
                reason=str(e)
                if self.defaut_payload!=None:
                    content=self.defaut_payload
                else:
                    content="".encode()
                headers=self.defaut_headers
            req=reqq
            return req

    def to_string(self):
        text=self.defaut_method + "\n"
        text= text + self.url()
        if len(self.defaut_headers)>0:
            text= text + "\n" + str(self.defaut_headers)
        if self.defaut_payload!=None:
            if len(self.defaut_payload)>0:
                text= text + "\n" + str(self.defaut_payload)
        return text
    
def new(curlfile):
    with open(curlfile, 'r') as file:
        data = file.read().replace(" --location","")
        data = data.replace(" --url","")
        uncurled =curlparser.parse(data)
        headers=dict(uncurled.header)
        headersclean={}
        for key in headers:
            headersclean[key]=headers[key].strip()

        return ApiProps(uncurled.method, uncurled.url, headersclean, uncurled.data)
def new_from_text(text):
    data =  text.replace(" --location","")
    data = data.replace(" --url","")
    uncurled =curlparser.parse(data)
    headers=dict(uncurled.header)
    headersclean={}
    for key in headers:
        headersclean[key]=headers[key].strip()

    return ApiProps(uncurled.method, uncurled.url, headersclean, uncurled.data)