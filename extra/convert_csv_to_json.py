import csv
import json

def convert_csv_to_json(csv_file, json_file):
    data = []
    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file, delimiter=';')
        for row in reader:
            issue_key = row['Issue key']
            issue_id = row['Issue id']
            custom_field = row['Custom field (Tickets N°)']
            
            issue_data = {
                'key': issue_key,
                'id': issue_id,
                'fields': {
                    'customfield_10077': custom_field
                }
            }
            data.append(issue_data)

    with open(json_file, 'w') as file:
        json.dump(data, file, indent=2)

# Usage example
csv_file = 'archives/Export_VKB_Active.csv'
json_file = 'archives/Export_VKB_Active.json'
convert_csv_to_json(csv_file, json_file)