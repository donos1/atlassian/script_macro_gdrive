import api_props
from api_props import UrlParts
import json
from api_props import ApiProps
import urllib.parse
import xml.etree.ElementTree as ET
from lxml import etree
import globals
from globals import logj
from globals import verbose
from globals import log_request_error
import os
from xml.dom import minidom
from jsonpath_rw import jsonpath, parse
import inspect
import xml.dom.minidom
import re

import jsonpath_ng.ext as jp

def is_empty(obj):
    if isinstance(obj, dict):
        return all(is_empty(v) for v in obj.values())
    elif isinstance(obj, list):
        return all(is_empty(item) for item in obj)
    else:
        return False  # Non-empty value

#Extract all from kofile.tpondemand.com except Test case, THEN ONLY TESTCASE
#Via API


#old implementation
#def xpath_to_jsonpath(xpath):
#    jsonpath = xpath.replace('/', '.').replace('[', '.').replace(']', '').replace("@","")
#    jsonpath = jsonpath.replace('..', '.')
#    jsonpath = jsonpath.rstrip('.')
#    return jsonpath


def xpath_to_jsonpath(xpath):
    jsonpath = xpath.replace('/', '.')
    jsonpath = re.sub(r'\[([0-9]+)\]', r'.\1', jsonpath)
    jsonpath = re.sub(r'\[@([a-zA-Z0-9_-]+)\]', r'[\1]', jsonpath)
    return jsonpath


def api_extract_to_file(api, findall_xpath, xpath_next=None, verbose_output=True, log={}, inputfile= 'data.xml', encoding="utf-8"):
    output =verbose(verbose_output)
    content = None

    try:
        output.printv('Calling API ...')
        request = api.request()
        content_type = request.headers.get('content-type').lower()
        encoding =request.encoding
        content= request.text


        if request.status_code==200:
            if os.path.exists(inputfile):
                output.printv("Storing to file: " + inputfile)
                content_type =  os.path.splitext(inputfile)[-1].lower().replace(".","")

                new_xml_root = etree.fromstring(content)
                current_xml_root = etree.parse(inputfile).getroot()
                current_xml_root = update_xml_params(new_xml_root, current_xml_root)
                updated_xml = update_xml_element(current_xml_root, new_xml_root)
                ET.ElementTree(updated_xml).write(encoding=encoding, file_or_filename=inputfile)
                            
            else:
                output.printv("Storing to file: " + inputfile)
                with open(inputfile, 'w', encoding=encoding) as file:
                    # Write the content to the file
                    file.write(content)

        else:
            
            output.printv("Error calling: " + api.url.base_url() + api.url.path + "\n" + content)
            log["error.action"]="extract_all query"
            log["error.reason"]=str(content)
            logj(log, "archives/log_failed.json")
            content = None
            
    except Exception as e:
        output.printv(e)
        log["error.action"]="extract_all query"
        log["error.reason"]=str(e)
        logj(log, "archives/log_failed.json")
        content = None

    if content:
        if  "xml" in content_type:
            root = ET.fromstring(content)

            if xpath_next:

                next_parameter = None
                # Regular expression pattern to capture the attribute name
                pattern = r".*@(\w*)"
                # Find the attribute name using regex
                match = re.search(pattern, xpath_next)
                # Extract the attribute name
                if match:
                    attribute_name = match.group(1)

                    if attribute_name in root.attrib:
                        next_parameter =  root.attrib[attribute_name]
                    if root.find(xpath_next.replace("@" + attribute_name, "")):
                        if attribute_name in root.find(xpath_next.replace("@" + attribute_name, "")).attrib:
                            next_parameter =  root.find(xpath_next.replace("@" + attribute_name, "")).attrib[attribute_name]
                else:
                    if root.find(xpath_next):
                        next_parameter =  root.find(xpath_next).text.strip()

                if next_parameter :
                    next_url_parts = UrlParts(next_parameter)
                    for par in api.url.query:
                        if par in next_url_parts.query:
                            api.url.query[par] = next_url_parts.query[par]
                        else:
                            next_url_parts.query[par] = api.url.query[par]
                    api.url.query = next_url_parts.query

                    output.printv('Calling API next page ...')

                    api_extract_to_file(api, findall_xpath, xpath_next, verbose_output, log, inputfile, encoding)



        else:
            log["error.action"]=inspect.currentframe().f_code.co_name
            log["error.reason"]="content type not suported: " + content_type
            logj(log, "archives/log_failed.json")


def json_find_all(obj_json, findall):
    items =[]
    query = jp.parse(findall)
    for match in query.find(obj_json):
        value = match.value
        if not is_empty(value):
            items.append(value)
    return items


def api_extract_to_file2(api, findall, while_callback, next_callback, content_callback=None, content_callback_args_list =[], verbose_output=True, log={}, output_file_cache= None, encoding="utf-8"):
    output =verbose(verbose_output)
    content = None
    content_type =None

    #while the callback returns true...
    while while_callback(api) :

        output.printv('Calling API ...')
        try:
            request = api.request()
            content_type = request.headers.get('content-type').lower()
            encoding =request.encoding
            content= request.text
            if content_callback !=None:

                callback_response = content_callback(api, [content_callback_args_list])
                if isinstance(callback_response, ApiProps):
                    api = callback_response



        except Exception as e:
            output.printv(e)
            log["error.action"]="extract_all query"
            log["error.reason"]=str(e)
            logj(log, "archives/log_failed.json")
            content = None
            return

        if request.status_code!=200:
            output.printv("Error calling: " + api.url.base_url() + api.url.path + "\n" + content)
            log["error.action"]="extract_all query"
            log["error.reason"]=str(content)
            logj(log, "archives/log_failed.json")
            content = None
            return

        if not("xml" in content_type or "json" in content_type):
            log["error.action"]=inspect.currentframe().f_code.co_name
            log["error.reason"]="content type not suported: " + content_type
            logj(log, "archives/log_failed.json")
            return

        if output_file_cache != None: 
            if os.path.exists(output_file_cache):
                output.printv("Storing to file: " + output_file_cache)
                if content_type==None:
                    content_type =  os.path.splitext(output_file_cache)[-1].lower().replace(".","")

                if  "xml" in content_type:
                    new_xml_root = etree.fromstring(content)
                    current_xml_root = etree.parse(output_file_cache).getroot()
                    current_xml_root = update_xml_params(new_xml_root, current_xml_root)
                    updated_xml = update_xml_element(current_xml_root, new_xml_root)
                    ET.ElementTree(updated_xml).write(encoding=encoding, file_or_filename=output_file_cache)

                elif "json" in content_type:
                    obj_json = json.loads(content)
                    if not isinstance(obj_json, list):
                        items = json_find_all(obj_json, findall)
                    else:
                        items = obj_json

                    current_items =[]
                    if os.path.exists(output_file_cache):
                        with open(output_file_cache, 'r') as f:
                            file_content = f.read()
                        current_obj_json= json.loads(file_content)
                        if not isinstance(current_obj_json, list):
                            current_items = json_find_all(current_obj_json, findall)
                        else:
                            current_items = current_obj_json


                    current_items = current_items + items
                    output.printv(f"Current items {str(len(current_items))}")
                    file_contents= json.dumps(current_items)
                    with open(output_file_cache, 'w') as outfile:
                        outfile.write(file_contents)

            
            else:
                output.printv("Storing to file: " + output_file_cache)
                
                with open(output_file_cache, 'w') as outfile:
                    if "json" in content_type:
                        content_list= json_find_all(json.loads(content), findall)
                        file_contents = json.dumps(content_list)
                        outfile.write(file_contents)
                    else:
                        outfile.write(content)


        api = next_callback(api)
        if while_callback(api):
            output.printv('Calling API next page ...')
            #api_extract_to_file2(api, findall, while_callback, next_callback, verbose_output, log, output_file_cache, encoding)




def api_extract2(api, findall, while_callback, next_callback, content_callback=None, content_callback_args_list =[], verbose_output=True, log={}, cache_file= None, encoding="utf-8"):
    output =verbose(verbose_output)
    all_items=[]
    total_result = None
    #check if there is a cached file
    has_cache_file = False
    if cache_file != None:
        has_cache_file = os.path.exists(cache_file)

    if has_cache_file:
        #loading a cached file
        output.printv("Using cache file: " + cache_file)
        content_type = os.path.splitext(cache_file)[-1].lower().replace(".","")

        if content_type == "xml":
            total_result = ET.parse(cache_file)
            if total_result:
                all_items = total_result.findall(findall)
        elif content_type == "json":

            with open(cache_file, 'r') as f:
                file_content = f.read()
            current_obj_json= json.loads(file_content)

            if not isinstance(current_obj_json, list):
                all_items = json_find_all( current_obj_json, findall)
            else:
                all_items = current_obj_json
        else:
            log["error.action"]=inspect.currentframe().f_code.co_name
            log["error.reason"]="content type not suported: " + os.path.splitext(cache_file)[-1].lower()
            logj(log, "archives/log_failed.json")
    else:
        #if there is not a cached file then generate it
        #call api extraction to file
        api_extract_to_file2(api, findall, while_callback, next_callback, content_callback, content_callback_args_list, verbose_output, log, cache_file, encoding)
        content_type = os.path.splitext(cache_file)[-1].lower().replace(".","")
        if cache_file != None:
            has_cache_file = os.path.exists(cache_file)
        #check if the api_extract_to_file2 managed to generate a file if yes then load it 
        if has_cache_file:
            all_items = api_extract2(api, findall, while_callback, next_callback, content_callback, content_callback_args_list, verbose_output, log, cache_file, encoding)


    if has_cache_file:
        output.printv("Total items: " + str(len(all_items)))
        return all_items 
    else:
        return None




def api_extract(api, findall_xpath, xpath_next=None, verbose_output=True, log={}, cache_file= 'data.xml', encoding="utf-8"):
    output =verbose(verbose_output)
    all_items=[]
    total_result = None
    if os.path.exists(cache_file):
        output.printv("Using cache file: " + cache_file)
        if os.path.splitext(cache_file)[-1].lower() == ".xml":
            total_result = ET.parse(cache_file)
        else:
            log["error.action"]=inspect.currentframe().f_code.co_name
            log["error.reason"]="content type not suported: " + os.path.splitext(cache_file)[-1].lower()
            logj(log, "archives/log_failed.json")
    else:
        api_extract_to_file(api, findall_xpath, xpath_next, verbose_output, log, cache_file, encoding)
        if os.path.exists(cache_file):
            if os.path.splitext(cache_file)[-1].lower() == ".xml":
                total_result = ET.parse(cache_file)
            else:
                log["error.action"]=inspect.currentframe().f_code.co_name
                log["error.reason"]="content type not suported: " + os.path.splitext(cache_file)[-1].lower()
                logj(log, "archives/log_failed.json")

    if total_result:
        all_items = total_result.findall(findall_xpath)
    output.printv("Total items: " + str(len(all_items)))
    return all_items  

def api_extract_to_xml(api, findall, verbose_output=True, log={}, inputfile= 'data.xml'):
    output =verbose(verbose_output)
    items = ET.Element("items")

    try:

        if os.path.exists(inputfile):
            print("Using cache file: " + inputfile)
            total_result = ET.parse(inputfile)
            

        else:
            print('Getting items ...')
            request = api.request()

            if request.status_code==200:
                root = ET.fromstring(request.text)
                while "Next" in root.attrib:
                    print("Got " + str(len(root.findall(findall))) + " items. Appending to list...")
                    for child in root.findall(findall):
                        items.append(child)

                    next_url_parts = UrlParts(root.attrib["Next"])
                    api.url.query["take"]=next_url_parts.query["take"]
                    api.url.query["skip"]=next_url_parts.query["skip"]
                    print('Getting next page of items ...')
                    request = api.request()
                    root = ET.fromstring(request.text)

                print("Got " + str(len(root.findall(findall))) + " items. Appending to list...")
                for child in root.findall(findall):
                    items.append(child)
                total_result = ET.ElementTree(items)
            else:
                log_request_error(output, log, request, "extract_all")


    except Exception as e:
        output.printv(e)
        log["error.action"]="extract_all query"
        log["error.reason"]=str(e)
        logj(log, "archives/log_failed.json")
        
    print("Total items: " + str(len(total_result.findall(findall))))
    return total_result


def url_encode(url):
    return urllib.parse.quote(url, safe='=&/:=?_()\'')


def update_xml_params(new_xml_element, current_xml_element):
    # Element exists in current XML, check attributes
    for attr_name, attr_value in new_xml_element.attrib.items():
        # Check if attribute exists in current element
        if attr_name not in current_xml_element.attrib:
            # Attribute does not exist, add it
            current_xml_element.attrib[attr_name] = attr_value
        elif current_xml_element.attrib[attr_name] != attr_value:
            # Attribute value is different, update it
            current_xml_element.attrib[attr_name] = attr_value
    return current_xml_element


def update_xml_element(root_element, new_items_element, find_all="./*"):
    all_elements= new_items_element.findall(find_all)
    for new_xml_element in all_elements:
        last_field = len(root_element.findall(find_all))
        root_element.insert(last_field, new_xml_element)
    return root_element



def xml_to_json(element):
    data = {}
    data[element.tag] = {}
    if len(element)>0:
        for child in element:
            data[element.tag]=element.attrib

            if child.text is not None and child.text.strip():
                data[element.tag][child.tag]=child.text.strip()
            else:
                if not child.tag in data[element.tag]:
                    data[element.tag][child.tag]=[]
                data[element.tag][child.tag].append(xml_to_json(child)[child.tag])
    else:
        for attrib, val in element.attrib.items():
            data[element.tag][attrib]=val

    return data



def json_to_xml(data, parent=None):
    if isinstance(data, dict):
        for tag, value in data.items():
            if isinstance(value, dict):
                element = ET.SubElement(parent, tag)
                json_to_xml(value, parent=element)
            elif isinstance(value, list):
                for item in value:
                    json_to_xml({tag: item}, parent=parent)
            else:
                element = ET.SubElement(parent, tag)
                element.text = str(value)
    else:
        parent.text = str(data)

    return parent

