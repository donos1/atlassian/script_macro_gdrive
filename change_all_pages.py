import json
from extra.api import *

def has_next_page_jira(api):
    if api.response !=None:
        has_result = len(json.loads(api.response.text))>0
        if not has_result:
            print("No more results")
    else:
        #is has noy called yet, so return true to do the first call
        return True
    return has_result


def next_call_jira(api):
    num_results =  len(json.loads(api.response.text))
    if "startAt" in api.url.query:
        api.url.query["startAt"]=int(api.url.query["startAt"]) + num_results
    else:
        api.url.query["startAt"]=num_results
    return api

def for_each_api_result(api, content_callback_args_list):

    #declaring api to change 
    jira_get_all_users_curl_file = "cur_files/conquest_jira_edit_page.txt"
    jira_api_edit_page = api_props.new(jira_get_all_users_curl_file)


    obj_json =  json.loads(api.response.text)

    if "results" in obj_json:
        for page in obj_json["results"]:
            if "body" in page:
                if 'storage' in page["body"]:
                    if 'value' in  page["body"]['storage']:
                        if 'gdrive' in page["body"]['storage']['value']:
                            page_content = page["body"]['storage']['value']

                            pattern = r'(<ac:structured-macro ac:name="lref-gdrive-file".*?<ac:parameter ac:name="url">(.*?)<\/ac:parameter>)'
                            matches = re.search(pattern, page_content, re.DOTALL)

                            for match in matches:
                                macro_tag =  match.group(1)
                                url =  match.group(2)
                                link_tag = '<a href=\"' + url + '\">' + url +  '</a>'
                                updated_page_value = page_content.replace(macro_tag, link_tag)
                                json_default_payload = json.loads(jira_api_edit_page.defaut_payload)
                                json_default_payload["body"]['storage']["value"] = updated_page_value
                                jira_api_edit_page.defaut_payload = str(json_default_payload)
                                
                                print("Changing a macro 'lref-gdrive-file' on Page ID: " + page["id"])

                                ################
                                ####   UNCOMENT THE LINE BELLOW TO REALLY DO THE CHANGES
                                #jira_api_edit_page.request()


jira_get_all_users_curl_file = "cur_files/conquest_jira_get_all_pages.txt"

#########################################################################
#########################################################################
#GETTING USERS ID

print("Getting Jira Users")
jira_api = api_props.new(jira_get_all_users_curl_file)
jira_users_cache_file= "/Users/kyliangallot/Développement/script_update_auto/script_macro_gdrive/jira_pages.json"
all_jira_pages = api_extract2(jira_api, "$.results.[*]", has_next_page_jira, next_call_jira, for_each_api_result)